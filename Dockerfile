# Based on work from https://github.com/vladgolubev/docker-libreoffice-pdf-cli

FROM debian:bullseye

# Disable prompts on apt-get install
ENV DEBIAN_FRONTEND noninteractive

# Install latest stable LibreOffice
RUN apt-get update -qq \
    && apt-get install -y -q libreoffice \
    && apt-get remove -q -y libreoffice-gnome \
    && apt-get install -y -q pdftk

# Cleanup after apt-get commands
RUN apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /var/cache/apt/archives/*.deb /var/cache/apt/*cache.bin

# Create user 'converter'
RUN useradd --create-home --shell /bin/bash converter \
    # Give user right to run libreoffice binary
    && chown converter:converter /usr/bin/libreoffice

COPY ./assets/fonts/* /usr/local/share/fonts/
COPY ./assets/fonts/* /usr/share/fonts/

RUN dpkg-reconfigure fontconfig-config

ADD ./src/YaRSfBB2020.ods /home/converter

RUN chown converter:converter /home/converter/YaRSfBB2020.ods

USER converter
WORKDIR /home/converter


# Write PDF
CMD libreoffice --invisible --nologo --convert-to pdf:"writer_pdf_Export:SelectPdfVersion=2" --outdir $(pwd) ./YaRSfBB2020.ods \
    && cat ./YaRSfBB2020.pdf | pdftk - cat 1-4 output -
