# YaRSfBB2020

Yet another Reference Sheet for BB2020.

This has to do with [Blood Bowl](https://www.bloodbowl.com/), a board game created by [Games Workshop](https://www.games-workshop.com). If you do not have the official rules, it will not help you. So you should buy the official rules from them or any retailer.

## Changelog

See [changelog.md](changelog.md).

## Release

Latest "Release" (i.e latest generated PDF) should be found [here](https://gitlab.com/chwa/YaRSfBB2020/-/jobs/artifacts/main/download?job=generate_job).
