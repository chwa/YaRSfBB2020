#!/bin/bash

RELEASE=$(git describe --abbrev=0 --tags)
PREVIOUS_RELEASE=$(git describe --abbrev=0 --tags ${RELEASE}~)

cat changelog.md | awk '/'${RELEASE}'/{f=1} /'${PREVIOUS_RELEASE}'/{f=0} f'
