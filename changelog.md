# [YaRSfBB2020](readme.md) Changelog

## 20240213

- Added Bloodlust
- Fixed embedding of fonts
- Added clarification for rerolling of random skills
- Added clarification on how to use inducements in Pre-Game sequence

## 20220927

- Added new "Hit and run" trait

## 20220706

- Added QR-Code with URL to latest release.
- Fix #2: Corrected Ball&Chain, and clarified Ball&Chain
- Fix #3: Added page numbers to Skill titles

## 20220619

- Changed colors for skill categories to match generally used ones (blue=general, red=strength, etc.)

## 20220523

- Added/fixed missing modificator for superb TTM landing roll.
- Clarified Pro Skill.

## 20220513

- Added Drunkart and Pick-Me-Up

## 20211130

- Adjusted according to [Designer's Commentary November 2021](https://www.warhammer-community.com/wp-content/uploads/2017/11/H8ph24vl1tTcR0hr.pdf)

## 20210809

- 1st version
